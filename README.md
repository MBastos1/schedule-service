# Gestão Integral da Saúde do Associado (GISA)

A arquitetura de integração, denominada **Gestão Integral da Saúde do Associado (GISA)**, é uma proposta arquitetural
completa e totalmente integrada, que está sendo construída pela equipe técnica da Boa Saúde. GISA é uma solução
constituída de quatro módulos funcionais, que deve incorporar e/ou integrar os recursos existentes nos sistemas SAF,
SGPS e SAS por meio de quatro módulos:

**I. Módulo de Informações Cadastrais:** trata-se de um módulo cujo escopo consiste em obter e manter informações de
todos os prestadores e associados, dentre as quais se destacam: informações pessoais, de localização, de formação, de
saúde e outras necessárias ao negócio da empresa. Essas informações têm como fonte os próprios prestadores e associados,
dados existentes em sistemas médicos e registros de consultas e exames, etc;

**II. Módulo de Serviços ao Associado:** esta parte do sistema é baseada numa solução de workflow, com o uso de Business
Process Management – BPM. Por meio deste módulo é possível desenhar, analisar e acompanhar todos os processos existentes
na empresa - tanto os já existentes quanto os que ainda serão implantados, desta forma melhorando o desempenho e a
eficiência desses processos;

**III. Módulo de Gestão e Estratégia:** tem como escopo prover a gestão estratégica de todos os projetos, produtos e
serviços da empresa, com indicadores do andamento individual e global dos projetos e serviços, na forma de um cockpit.
Para este módulo será utilizada uma ferramenta de gestão corporativa adquirida no mercado;

**IV. Módulo de Ciência de Dados - Data Warehouse (DW) e Business Intelligence (BI):** este módulo do sistema deve
utilizar ferramentas adequadas para obtenção, guarda, recuperação e utilização dos dados corporativos pertinentes, com
recursos para tratamento de dados massivos (Big Data), mineração dos dados de saúde e apoio às tomadas de decisão. Todos
os dados deste módulo são obtidos de planilhas e Sistemas Gerenciadores de Bancos de Dados (SGBDs), relacionais ou
noSQL. O uso de recursos de um Data Warehouse (DW), nesse contexto, é essencial para o sucesso desta iniciativa;
