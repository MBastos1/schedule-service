FROM openjdk:11-jdk-oraclelinux8
LABEL maintainer="pucminas.com"

VOLUME /tmp
EXPOSE 8082

ARG JAR_FILE=target/schedule-service*.jar
ADD ${JAR_FILE} schedule-service.jar

ENTRYPOINT ["sh", "-c", "java -jar /schedule-service.jar"]

