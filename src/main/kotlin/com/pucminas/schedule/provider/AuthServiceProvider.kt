package com.pucminas.schedule.provider

import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody

@FeignClient(
    value = "auth",
    url = "http://\${AUTH_SERVICE_URL:localhost}:8081"
)
interface AuthServiceProvider {

//    @PostMapping("/api/v1/user")
//    fun saveUser(@RequestBody userRequest: UserRequest): UserResponse
//
//    @GetMapping("/api/v1/user/{userId}")
//    fun findById(@PathVariable userId: String): UserResponse
}