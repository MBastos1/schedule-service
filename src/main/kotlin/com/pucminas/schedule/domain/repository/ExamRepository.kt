package com.pucminas.schedule.domain.repository

import com.pucminas.schedule.domain.base.BaseRepository
import com.pucminas.schedule.domain.entity.Exam
import org.springframework.stereotype.Repository

@Repository
interface ExamRepository : BaseRepository<Exam, String>