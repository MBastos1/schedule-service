package com.pucminas.schedule.domain.repository

import com.pucminas.schedule.domain.base.BaseRepository
import com.pucminas.schedule.domain.entity.Appointment
import org.springframework.stereotype.Repository

@Repository
interface AppointmentRepository : BaseRepository<Appointment, String>