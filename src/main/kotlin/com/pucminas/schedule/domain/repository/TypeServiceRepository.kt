package com.pucminas.schedule.domain.repository

import com.pucminas.schedule.domain.base.BaseRepository
import com.pucminas.schedule.domain.entity.TypeService
import org.springframework.stereotype.Repository

@Repository
interface TypeServiceRepository : BaseRepository<TypeService, String>