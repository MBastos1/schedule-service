package com.pucminas.schedule.domain.repository

import com.pucminas.schedule.domain.base.BaseRepository
import com.pucminas.schedule.domain.entity.Plan
import org.springframework.stereotype.Repository

@Repository
interface PlanRepository: BaseRepository<Plan, String>