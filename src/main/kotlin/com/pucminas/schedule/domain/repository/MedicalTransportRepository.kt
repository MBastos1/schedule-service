package com.pucminas.schedule.domain.repository

import com.pucminas.schedule.domain.base.BaseRepository
import com.pucminas.schedule.domain.entity.MedicalTransport

interface MedicalTransportRepository : BaseRepository<MedicalTransport, String>