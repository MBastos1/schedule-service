package com.pucminas.schedule.domain.repository

import com.pucminas.schedule.domain.base.BaseRepository
import com.pucminas.schedule.domain.entity.Schedule

interface ScheduleRepository : BaseRepository<Schedule, String>