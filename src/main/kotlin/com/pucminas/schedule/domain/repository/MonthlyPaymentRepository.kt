package com.pucminas.schedule.domain.repository

import com.pucminas.schedule.domain.base.BaseRepository
import com.pucminas.schedule.domain.entity.HealthInsurance
import com.pucminas.schedule.domain.entity.MonthlyPayment
import org.springframework.stereotype.Repository

@Repository
interface MonthlyPaymentRepository: BaseRepository<MonthlyPayment, String>