package com.pucminas.schedule.domain.repository

import com.pucminas.schedule.domain.base.BaseRepository
import com.pucminas.schedule.domain.entity.HealthInsurance
import org.springframework.stereotype.Repository

@Repository
interface HealthInsuranceRepository : BaseRepository<HealthInsurance, String>