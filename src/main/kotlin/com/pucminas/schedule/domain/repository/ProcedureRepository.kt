package com.pucminas.schedule.domain.repository

import com.pucminas.schedule.domain.base.BaseRepository
import com.pucminas.schedule.domain.entity.Procedure
import org.springframework.stereotype.Repository

@Repository
interface ProcedureRepository : BaseRepository<Procedure, String>