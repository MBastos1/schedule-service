package com.pucminas.schedule.domain.repository

import com.pucminas.schedule.domain.entity.Company
import com.pucminas.schedule.domain.base.BaseRepository
import org.springframework.stereotype.Repository

@Repository
interface CompanyRepository : BaseRepository<Company, String>