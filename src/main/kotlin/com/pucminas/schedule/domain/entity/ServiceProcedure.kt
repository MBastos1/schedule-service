package com.pucminas.schedule.domain.entity

import org.hibernate.annotations.CreationTimestamp
import java.time.LocalDateTime
import java.util.*
import javax.persistence.*

@Entity
data class ServiceProcedure (
    @Id
    @Column(updatable = false, nullable = false, length = 36)
    val id: String = UUID.randomUUID().toString(),

    @ManyToOne(optional = true, fetch = FetchType.LAZY, cascade = [CascadeType.ALL])
    val procedure: Procedure,

    @ManyToOne(optional = true, fetch = FetchType.LAZY, cascade = [CascadeType.ALL])
    val schedule: Schedule,

    @Column(length = 200, nullable = false)
    val description: String,

    @Column(length = 20, nullable = false)
    val code: String,

    @CreationTimestamp
    @Column(updatable = false)
    val createdAt: LocalDateTime? = null
)