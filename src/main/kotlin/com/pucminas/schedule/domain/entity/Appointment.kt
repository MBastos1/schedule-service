package com.pucminas.schedule.domain.entity

import java.time.LocalDateTime
import java.util.*
import javax.persistence.*

@Entity
data class Appointment (
    @Id
    @Column(updatable = false, nullable = false, length = 36)
    val id: String = UUID.randomUUID().toString(),

    @Column(length = 36, nullable = false)
    val providerId: String,

    @ManyToOne(optional = true, fetch = FetchType.LAZY, cascade = [CascadeType.ALL])
    val schedule: Schedule,

    @Column(length = 200, nullable = true)
    var note: String? = null,

    @Column(nullable = true)
    var dateTimeService: LocalDateTime,

    @Column(nullable = false)
    val isCallCenter: Boolean = true
)