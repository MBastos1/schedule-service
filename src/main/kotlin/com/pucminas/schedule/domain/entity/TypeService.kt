package com.pucminas.schedule.domain.entity

import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.UpdateTimestamp
import java.time.LocalDateTime
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id

@Entity
data class TypeService (
    @Id
    @Column(updatable = false, nullable = false, length = 36)
    val id: String = UUID.randomUUID().toString(),

    @Column(length = 50, nullable = false)
    val description: String,

    @Column(length = 150, nullable = false)
    val detail: String,

    @Column(nullable = false)
    val isActive: Boolean = true,

    @CreationTimestamp
    @Column(updatable = false)
    val createdAt: LocalDateTime? = null,

    @UpdateTimestamp
    @Column(updatable = true, insertable = false, nullable = true)
    var updatedAt: LocalDateTime? = null
)