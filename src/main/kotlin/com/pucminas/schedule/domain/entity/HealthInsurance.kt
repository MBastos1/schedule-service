package com.pucminas.schedule.domain.entity

import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.UpdateTimestamp
import java.time.LocalDateTime
import java.util.*
import javax.persistence.*

@Entity
data class HealthInsurance (

    @Id
    @Column(updatable = false, nullable = false, length = 36)
    val id: String = UUID.randomUUID().toString(),

    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = [CascadeType.ALL])
    val plan: Plan,

    @Column(length = 36, nullable = false)
    val associateId: String,

    @ManyToOne(optional = true, fetch = FetchType.LAZY, cascade = [CascadeType.ALL])
    val company: Company?,

    @Column(length = 200, nullable = true)
    var note: String? = null,

    @Column(nullable = false)
    val isActive: Boolean = true,

    @Column(nullable = false)
    val appointmentsServiceLimit: Int,

    @Column(nullable = false)
    val serviceLimitExams: Int,

    @Column(nullable = false)
    val contractStartDate: LocalDateTime,

    @Column(nullable = true)
    val contractEndDate: LocalDateTime? = null,

    @Column(nullable = false)
    val isRenewedContract: Boolean = true,

    @CreationTimestamp
    @Column(updatable = false)
    val createdAt: LocalDateTime? = null,

    @UpdateTimestamp
    @Column(updatable = true, insertable = false, nullable = true)
    var updatedAt: LocalDateTime? = null
)