package com.pucminas.schedule.domain.entity

import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.UpdateTimestamp
import java.time.LocalDateTime
import java.util.*
import javax.persistence.*

@Entity
class MedicalTransport (
    @Id
    @Column(updatable = false, nullable = false, length = 36)
    val id: String = UUID.randomUUID().toString(),

    @Column(length = 36, nullable = false)
    val providerId: String,

    @ManyToOne(optional = true, fetch = FetchType.LAZY, cascade = [CascadeType.ALL])
    val schedule: Schedule,

    @Column(length = 200, nullable = true)
    var note: String? = null,

    @Column(nullable = true)
    var dateTimeFinalized: LocalDateTime,

    @Column(length = 200, nullable = true)
    var street: String? = null,

    @Column(length = 200, nullable = true)
    var complement: String? = null,

    @Column(length = 20, nullable = true)
    val latitude: String?,

    @Column(length = 20, nullable = true)
    val longitude: String?,

    @CreationTimestamp
    @Column(updatable = false)
    val createdAt: LocalDateTime? = null,

    @UpdateTimestamp
    @Column(updatable = true, insertable = false, nullable = true)
    var updatedAt: LocalDateTime? = null
)