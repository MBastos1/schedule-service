package com.pucminas.schedule.domain.entity

import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.UpdateTimestamp
import java.time.LocalDateTime
import java.util.*
import javax.persistence.*

@Entity
data class Schedule (
    @Id
    @Column(updatable = false, nullable = false, length = 36)
    val id: String = UUID.randomUUID().toString(),

    @ManyToOne(optional = true, fetch = FetchType.LAZY, cascade = [CascadeType.ALL])
    val typeService: TypeService,

    @Column(length = 36, nullable = false)
    val partneredId: String,

    @ManyToOne(optional = true, fetch = FetchType.LAZY, cascade = [CascadeType.ALL])
    val healthInsurance: HealthInsurance,

    @Column(nullable = false)
    val collaboratorRegistration: Int,

    @Column(nullable = false)
    val changeCollaborator: Int,

    @Column(nullable = false)
    val collaboratorAuthorizedDenied: Int,

    @Column(nullable = false)
    val isReturn: Boolean = true,

    @Column(length = 200, nullable = true)
    var note: String? = null,

    @Column(nullable = true)
    var dateTimeSchedule: LocalDateTime? = null,

    @Column(nullable = false)
    val isAuthorized: Boolean = true,

    @Column(nullable = false)
    val isAccomplished: Boolean = true,

    @Column(nullable = false)
    val isEmergency: Boolean = true,

    @CreationTimestamp
    @Column(updatable = false)
    val createdAt: LocalDateTime? = null,

    @UpdateTimestamp
    @Column(updatable = true, insertable = false, nullable = true)
    var updatedAt: LocalDateTime? = null
)