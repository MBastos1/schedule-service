package com.pucminas.schedule.domain.entity

import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.UpdateTimestamp
import java.time.LocalDateTime
import java.util.*
import javax.persistence.*

@Entity
data class MonthlyPayment (
    @Id
    @Column(updatable = false, nullable = false, length = 36)
    val id: String = UUID.randomUUID().toString(),

    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = [CascadeType.ALL])
    val healthInsurance: HealthInsurance,

    @Column(length = 200, nullable = true)
    var note: String? = null,

    @Column(nullable = false)
    val monthlyAmount: Double,

    @Column(nullable = false)
    val parcelNumber: Int,

    @Column(nullable = false)
    val dueDate: LocalDateTime,

    @Column(nullable = false)
    val paymentDate: LocalDateTime,

    @Column(nullable = false)
    val isActive: Boolean = true,

    @Column(nullable = false)
    val isPaid: Boolean = false,

    @Column(nullable = false)
    val extraPercentage: Int,

    @CreationTimestamp
    @Column(updatable = false)
    val createdAt: LocalDateTime? = null,

    @UpdateTimestamp
    @Column(updatable = true, insertable = false, nullable = true)
    var updatedAt: LocalDateTime? = null
)