package com.pucminas.schedule.domain.entity

import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.UpdateTimestamp
import java.time.LocalDateTime
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "plans")
data class Plan (
    @Id
    @Column(updatable = false, nullable = false, length = 36)
    val id: String = UUID.randomUUID().toString(),

    @Column(length = 50, nullable = false)
    val description: String,

    @Column(length = 1, nullable = false)
    val planType: String,

    @Column(length = 1, nullable = false)
    val relationshipType: String,

    @Column(length = 1, nullable = false)
    val segmentType: String,

    @Column(nullable = false)
    val isActive: Boolean = true,

    @CreationTimestamp
    @Column(updatable = false)
    val createdAt: LocalDateTime? = null,

    @UpdateTimestamp
    @Column(updatable = true, insertable = false, nullable = true)
    var updatedAt: LocalDateTime? = null
)