package com.pucminas.schedule.domain.entity

import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.UpdateTimestamp
import java.time.LocalDateTime
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id

@Entity
data class Company(
    @Id
    @Column(updatable = false, nullable = false, length = 36)
    val id: String = UUID.randomUUID().toString(),

    @Column(length = 50, nullable = false)
    val socialReason: String,

    @Column(length = 50, nullable = false)
    val fantasyName: String,

    @Column(length = 14, nullable = false)
    val cnpj: String,

    @Column(length = 100, nullable = false)
    val contactName: String,

    @Column(length = 100, nullable = false)
    val contactPhone: String,

    @Column(length = 200, nullable = false)
    val address: String,

    @Column(nullable = false)
    val isActive: Boolean = true,
){
    @CreationTimestamp
    @Column(updatable = false)
    lateinit var createdAt: LocalDateTime

    @UpdateTimestamp
    @Column(updatable = true, insertable = false, nullable = true)
    var updatedAt: LocalDateTime? = null
}