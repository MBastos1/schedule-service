package com.pucminas.schedule.domain.base

interface BaseService<T, ID> {
    fun findById(id: ID): T?
    fun findAll(): List<T>
}