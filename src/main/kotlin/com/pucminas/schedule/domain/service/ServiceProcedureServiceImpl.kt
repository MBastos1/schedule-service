package com.pucminas.schedule.domain.service

import com.pucminas.schedule.domain.entity.Procedure
import com.pucminas.schedule.domain.entity.Schedule
import com.pucminas.schedule.domain.repository.ProcedureRepository
import com.pucminas.schedule.domain.repository.ScheduleRepository
import com.pucminas.schedule.domain.repository.ServiceProcedureRepository
import com.pucminas.schedule.web.dto.request.ServiceProcedureRequest
import com.pucminas.schedule.web.dto.response.ServiceProcedureResponse
import com.pucminas.schedule.web.extension.toEntity
import com.pucminas.schedule.web.extension.toResponse
import com.pucminas.schedule.web.service.ServiceProcedureService
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException

@Service
class ServiceProcedureServiceImpl(
    private val serviceProcedureRepository: ServiceProcedureRepository,
    private val procedureRepository: ProcedureRepository,
    private val scheduleRepository: ScheduleRepository
) : ServiceProcedureService {

    override fun save(serviceProcedureRequest: ServiceProcedureRequest): ServiceProcedureResponse =
        runCatching {
            val (procedure, schedule) = Pair(
                findProcedureById(serviceProcedureRequest.procedureId),
                findScheduleById(serviceProcedureRequest.scheduleId)
            )
            serviceProcedureRepository.save(
                serviceProcedureRequest.toEntity(procedure, schedule)
            ).toResponse()
        }.getOrElse {
            throw it
        }

    override fun findById(id: String): ServiceProcedureResponse? =
        serviceProcedureRepository.findById(id).map {
            it.toResponse()
        }.orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "Tipo de procedimento $id não encontrado")
        }

    override fun findAll(): List<ServiceProcedureResponse> =
        serviceProcedureRepository.findAll().map {
            it.toResponse()
        }

    private fun findScheduleById(id: String): Schedule =
        scheduleRepository.findById(id).map {
            it
        }.orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "Agendamento $id não encontrado")
        }

    private fun findProcedureById(id: String): Procedure =
        procedureRepository.findById(id).map {
            it
        }.orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "Procedimento $id não encontrado")
        }

}