package com.pucminas.schedule.domain.service

import com.pucminas.schedule.domain.entity.Plan
import com.pucminas.schedule.domain.entity.Schedule
import com.pucminas.schedule.domain.repository.HealthInsuranceRepository
import com.pucminas.schedule.domain.repository.PlanRepository
import com.pucminas.schedule.web.dto.request.HealthInsuranceCreateRequest
import com.pucminas.schedule.web.dto.request.HealthInsuranceUpdateRequest
import com.pucminas.schedule.web.dto.response.HealthInsuranceResponse
import com.pucminas.schedule.web.extension.toEntity
import com.pucminas.schedule.web.extension.toResponse
import com.pucminas.schedule.web.service.HealthInsuranceService
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException

@Service
class HealthInsuranceServiceImpl(
    private val healthInsuranceRepository: HealthInsuranceRepository,
    private val planRepository: PlanRepository
) : HealthInsuranceService {

    override fun save(healthInsuranceCreateRequest: HealthInsuranceCreateRequest): HealthInsuranceResponse =
        runCatching {
            with(findPlanById(healthInsuranceCreateRequest.planId)) {
                healthInsuranceRepository.save(
                    healthInsuranceCreateRequest.toEntity(this)
                ).toResponse()
            }
        }.getOrElse {
            throw it
        }

    override fun edit(id: String, healthInsuranceUpdateRequest: HealthInsuranceUpdateRequest): HealthInsuranceResponse =
        healthInsuranceRepository.findById(id).map {
            with(findPlanById(healthInsuranceUpdateRequest.planId)) {
                healthInsuranceRepository.save(
                    healthInsuranceUpdateRequest.toEntity(id,this)
                ).toResponse()
            }
        }.orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "Convênio $id não encontrado")
        }

    override fun findById(id: String): HealthInsuranceResponse? =
        healthInsuranceRepository.findById(id).map {
            it.toResponse()
        }.orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "Plano $id não encontrada")
        }

    override fun findAll(): List<HealthInsuranceResponse> =
        healthInsuranceRepository.findAll().map {
            it.toResponse()
        }

    private fun findPlanById(id: String): Plan =
        planRepository.findById(id).map {
            it
        }.orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "Plano $id não encontrado")
        }
}