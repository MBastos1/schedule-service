package com.pucminas.schedule.domain.service

import com.pucminas.schedule.domain.repository.MedicalTransportRepository
import com.pucminas.schedule.web.dto.response.MedicalTransportResponse
import com.pucminas.schedule.web.extension.toResponse
import com.pucminas.schedule.web.service.MedicalTransportService
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException

@Service
class MedicalTransportServiceImpl(
    private val medicalTransportRepository: MedicalTransportRepository
) : MedicalTransportService {

    override fun findById(id: String): MedicalTransportResponse? =
        medicalTransportRepository.findById(id).map {
            it.toResponse()
        }.orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "Transporte médico $id não encontrado")
        }

    override fun findAll(): List<MedicalTransportResponse> =
        medicalTransportRepository.findAll().map {
            it.toResponse()
        }
}