package com.pucminas.schedule.domain.service

import com.pucminas.schedule.domain.repository.TypeServiceRepository
import com.pucminas.schedule.web.dto.response.TypeServiceResponse
import com.pucminas.schedule.web.extension.toResponse
import com.pucminas.schedule.web.service.TypeService
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException

@Service
class TypeServiceImpl(private val typeServiceRepository: TypeServiceRepository) : TypeService {

    override fun findById(id: String): TypeServiceResponse? =
        typeServiceRepository.findById(id).map {
            it.toResponse()
        }.orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "Tipo de atendimento $id não encontrada")
        }

    override fun findAll(): List<TypeServiceResponse> =
        typeServiceRepository.findAll().map {
            it.toResponse()
        }
}