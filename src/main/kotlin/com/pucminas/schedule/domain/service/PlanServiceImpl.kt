package com.pucminas.schedule.domain.service

import com.pucminas.schedule.domain.repository.PlanRepository
import com.pucminas.schedule.web.dto.response.PlanResponse
import com.pucminas.schedule.web.extension.toResponse
import com.pucminas.schedule.web.service.PlanService
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException

@Service
class PlanServiceImpl(private val planRepository: PlanRepository) : PlanService {

    override fun findById(id: String): PlanResponse? =
        planRepository.findById(id).map {
            it.toResponse()
        }.orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "Plano $id não encontrada")
        }

    override fun findAll(): List<PlanResponse> =
        planRepository.findAll().map {
            it.toResponse()
        }
}