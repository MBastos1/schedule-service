package com.pucminas.schedule.domain.service

import com.pucminas.schedule.domain.repository.MonthlyPaymentRepository
import com.pucminas.schedule.web.dto.response.MonthlyPaymentResponse
import com.pucminas.schedule.web.extension.toResponse
import com.pucminas.schedule.web.service.MonthlyPaymentService
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException

@Service
class MonthlyPaymentServiceImpl(
    private val monthlyPaymentRepository: MonthlyPaymentRepository
) : MonthlyPaymentService {

    override fun findById(id: String): MonthlyPaymentResponse? =
        monthlyPaymentRepository.findById(id).map {
            it.toResponse()
        }.orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "Mensalidade $id não encontrada")
        }

    override fun findAll(): List<MonthlyPaymentResponse> =
        monthlyPaymentRepository.findAll().map {
            it.toResponse()
        }
}