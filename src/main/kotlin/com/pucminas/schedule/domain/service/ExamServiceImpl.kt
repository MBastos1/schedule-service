package com.pucminas.schedule.domain.service

import com.pucminas.schedule.domain.entity.Schedule
import com.pucminas.schedule.domain.repository.ExamRepository
import com.pucminas.schedule.domain.repository.ScheduleRepository
import com.pucminas.schedule.web.dto.request.ExamRequest
import com.pucminas.schedule.web.dto.response.ExamResponse
import com.pucminas.schedule.web.dto.response.ScheduleResponse
import com.pucminas.schedule.web.extension.toEntity
import com.pucminas.schedule.web.extension.toResponse
import com.pucminas.schedule.web.service.ExamService
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException

@Service
class ExamServiceImpl(
    private val examRepository: ExamRepository,
    private val scheduleRepository: ScheduleRepository
) : ExamService {

    override fun save(examRequest: ExamRequest): ExamResponse =
        runCatching {
            with(findByScheduleById(examRequest.scheduleId)) {
                examRepository.save(examRequest.toEntity(null, this)).toResponse()
            }
        }.getOrElse {
            throw it
        }

    override fun edit(id: String, examRequest: ExamRequest): ExamResponse =
        examRepository.findById(id).map {
            with(findByScheduleById(examRequest.scheduleId)) {
                examRepository.save(examRequest.toEntity(id, this)).toResponse()
            }
        }.orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "Exame $id não encontrado")
        }

    override fun deleteById(id: String) {
        findById(id).let {
            examRepository.deleteById(id)
        }
    }

    override fun findById(id: String): ExamResponse? =
        examRepository.findById(id).map {
            it.toResponse()
        }.orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "Exame $id não encontrada")
        }

    override fun findAll(): List<ExamResponse> =
        examRepository.findAll().map {
            it.toResponse()
        }

    private fun findByScheduleById(id: String): Schedule =
        scheduleRepository.findById(id).map {
            it
        }.orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "Agendamento $id não encontrado")
        }
}