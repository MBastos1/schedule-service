package com.pucminas.schedule.domain.service

import com.pucminas.schedule.domain.repository.HealthInsuranceRepository
import com.pucminas.schedule.domain.repository.ScheduleRepository
import com.pucminas.schedule.domain.repository.TypeServiceRepository
import com.pucminas.schedule.web.dto.request.ScheduleRequestCreate
import com.pucminas.schedule.web.dto.request.ScheduleRequestUpdate
import com.pucminas.schedule.web.dto.response.ScheduleResponse
import com.pucminas.schedule.web.extension.toEntity
import com.pucminas.schedule.web.extension.toResponse
import com.pucminas.schedule.web.service.ScheduleService
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException

@Service
class ScheduleServiceImpl(
    private val scheduleRepository: ScheduleRepository,
    private val typeServiceRepository: TypeServiceRepository,
    private val healthInsuranceRepository: HealthInsuranceRepository
) : ScheduleService {

    override fun save(scheduleRequestCreate: ScheduleRequestCreate): ScheduleResponse =
        runCatching {
            val (typeService, healthInsurance) = findTypeServiceAndHealthInsurance(
                scheduleRequestCreate.typeServiceId,
                scheduleRequestCreate.healthInsuranceId
            )
            scheduleRepository.save(scheduleRequestCreate.toEntity(
                typeService.get(), healthInsurance.get()
            )).toResponse()
        }.getOrElse {
            throw it
        }

    override fun edit(id: String, scheduleRequestUpdate: ScheduleRequestUpdate): ScheduleResponse =
        scheduleRepository.findById(id).map {
            val (typeService, healthInsurance) = findTypeServiceAndHealthInsurance(
                scheduleRequestUpdate.typeServiceId,
                scheduleRequestUpdate.healthInsuranceId
            )
            scheduleRepository.save(scheduleRequestUpdate.toEntity(
                id, typeService.get(), healthInsurance.get()
            )).toResponse()
        }.orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "Agendamento $id não encontrado")
        }

    override fun findById(id: String): ScheduleResponse? =
        scheduleRepository.findById(id).map {
            it.toResponse()
        }.orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "Agendamento $id não encontrado")
        }

    override fun findAll(): List<ScheduleResponse> =
        scheduleRepository.findAll().map {
            it.toResponse()
        }

    private fun findTypeServiceAndHealthInsurance(typeServiceId: String, healthInsuranceId: String) =
        Pair(typeServiceRepository.findById(typeServiceId), healthInsuranceRepository.findById(healthInsuranceId))
}