package com.pucminas.schedule.domain.service

import com.pucminas.schedule.domain.repository.ProcedureRepository
import com.pucminas.schedule.web.dto.response.ProcedureResponse
import com.pucminas.schedule.web.extension.toResponse
import com.pucminas.schedule.web.service.ProcedureService
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException

@Service
class ProcedureServiceImpl(private val procedureRepository: ProcedureRepository) : ProcedureService {

    override fun findById(id: String): ProcedureResponse? =
        procedureRepository.findById(id).map {
            it.toResponse()
        }.orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "Procedimento $id não encontrado")
        }

    override fun findAll(): List<ProcedureResponse> =
        procedureRepository.findAll().map {
            it.toResponse()
        }
}