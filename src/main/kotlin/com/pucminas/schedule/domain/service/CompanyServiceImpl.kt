package com.pucminas.schedule.domain.service

import com.pucminas.schedule.domain.repository.CompanyRepository
import com.pucminas.schedule.web.dto.request.CompanyRequest
import com.pucminas.schedule.web.dto.response.CompanyResponse
import com.pucminas.schedule.web.extension.toResponse
import com.pucminas.schedule.web.service.CompanyService
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException

@Service
class CompanyServiceImpl(private val companyRepository: CompanyRepository) : CompanyService {

    override fun findById(id: String): CompanyResponse? =
        companyRepository.findById(id).map {
            it.toResponse()
        }.orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "Empresa $id não encontrada")
        }

    override fun findAll(): List<CompanyResponse> =
        companyRepository.findAll().map {
            it.toResponse()
        }
}