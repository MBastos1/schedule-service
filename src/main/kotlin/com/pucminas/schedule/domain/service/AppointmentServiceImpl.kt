package com.pucminas.schedule.domain.service

import com.pucminas.schedule.domain.repository.AppointmentRepository
import com.pucminas.schedule.web.dto.response.AppointmentResponse
import com.pucminas.schedule.web.extension.toResponse
import com.pucminas.schedule.web.service.AppointmentService
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException

@Service
class AppointmentServiceImpl(
    private val appointmentRepository: AppointmentRepository
) : AppointmentService{

    override fun findById(id: String): AppointmentResponse? =
        appointmentRepository.findById(id).map {
            it.toResponse()
        }.orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "Consulta $id não encontrada")
        }

    override fun findAll(): List<AppointmentResponse> =
        appointmentRepository.findAll().map {
            it.toResponse()
        }
}