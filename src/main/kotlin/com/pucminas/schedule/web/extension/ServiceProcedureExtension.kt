package com.pucminas.schedule.web.extension

import com.pucminas.schedule.domain.entity.Procedure
import com.pucminas.schedule.domain.entity.Schedule
import com.pucminas.schedule.domain.entity.ServiceProcedure
import com.pucminas.schedule.web.dto.request.ServiceProcedureRequest
import com.pucminas.schedule.web.dto.response.ServiceProcedureResponse
import java.util.*


fun ServiceProcedureRequest.toEntity(procedure: Procedure, schedule: Schedule) = ServiceProcedure(
    id = UUID.randomUUID().toString(),
    procedure = procedure,
    schedule = schedule,
    description = this.description,
    code = this.code
)

fun ServiceProcedure.toResponse() = ServiceProcedureResponse(
    this.id,
    this.procedure.toResponse(),
    this.schedule.toResponse(),
    this.description,
    this.code,
    this.createdAt
)