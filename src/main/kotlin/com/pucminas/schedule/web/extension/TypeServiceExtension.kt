package com.pucminas.schedule.web.extension

import com.pucminas.schedule.domain.entity.TypeService
import com.pucminas.schedule.web.dto.response.TypeServiceResponse

fun TypeService.toResponse() = TypeServiceResponse(
    id = this.id,
    description = this.description,
    detail = this.detail,
    isActive = this.isActive,
    createdAt = this.createdAt,
    updatedAt = this.updatedAt
)