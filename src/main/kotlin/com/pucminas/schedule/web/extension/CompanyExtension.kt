package com.pucminas.schedule.web.extension

import com.pucminas.schedule.domain.entity.Company
import com.pucminas.schedule.web.dto.request.CompanyRequest
import com.pucminas.schedule.web.dto.response.CompanyResponse
import java.util.*

fun CompanyRequest.toEntity(id: String?) = Company(
    id = id ?: UUID.randomUUID().toString(),
    socialReason = this.socialReason,
    fantasyName = this.fantasyName,
    cnpj = this.cnpj,
    contactName = this.contactName,
    contactPhone = this.contactPhone,
    address = this.address,
    isActive = this.isActive
)


fun Company.toResponse() = CompanyResponse(
    id = this.id,
    socialReason = this.socialReason,
    fantasyName = this.fantasyName,
    cnpj = this.cnpj,
    contactName = this.contactName,
    contactPhone = this.contactPhone,
    address = this.address,
    isActive = this.isActive,
    createdAt = this.createdAt,
    updatedAt = this.updatedAt
)