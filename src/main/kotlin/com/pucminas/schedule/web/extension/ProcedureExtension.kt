package com.pucminas.schedule.web.extension

import com.pucminas.schedule.domain.entity.Procedure
import com.pucminas.schedule.web.dto.response.ProcedureResponse

fun Procedure.toResponse() = ProcedureResponse(
    id, description, detail, code, createdAt, updatedAt
)