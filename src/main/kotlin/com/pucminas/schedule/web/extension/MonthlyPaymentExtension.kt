package com.pucminas.schedule.web.extension

import com.pucminas.schedule.domain.entity.MonthlyPayment
import com.pucminas.schedule.web.dto.response.MonthlyPaymentResponse

fun MonthlyPayment.toResponse() = MonthlyPaymentResponse(
    id = this.id,
    healthInsurance =  this.healthInsurance.toResponse(),
    note = this.note,
    monthlyAmount = this.monthlyAmount,
    parcelNumber = this.parcelNumber,
    dueDate = this.dueDate,
    paymentDate = this.paymentDate,
    isActive = this.isActive,
    isPaid = this.isPaid,
    extraPercentage = this.extraPercentage,
    createdAt = this.createdAt,
    updatedAt = this.updatedAt
)