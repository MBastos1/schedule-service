package com.pucminas.schedule.web.extension

import com.pucminas.schedule.domain.entity.Appointment
import com.pucminas.schedule.web.dto.response.AppointmentResponse

fun Appointment.toResponse() = AppointmentResponse(
    this.id,
    this.providerId,
    this.schedule.toResponse(),
    this.note,
    this.dateTimeService,
    this.isCallCenter
)