package com.pucminas.schedule.web.extension

import com.pucminas.schedule.domain.entity.Exam
import com.pucminas.schedule.domain.entity.Schedule
import com.pucminas.schedule.web.dto.request.ExamRequest
import com.pucminas.schedule.web.dto.response.ExamResponse
import java.util.*

fun ExamRequest.toEntity(id: String?, schedule: Schedule) = Exam(
    id = id ?: UUID.randomUUID().toString(),
    providerId = this.providerId,
    schedule = schedule,
    note = this.note,
    dateTimeService = this.dateTimeService
)

fun Exam.toResponse() = ExamResponse(
    this.id,
    this.providerId,
    this.schedule.toResponse(),
    this.note,
    this.dateTimeService
)