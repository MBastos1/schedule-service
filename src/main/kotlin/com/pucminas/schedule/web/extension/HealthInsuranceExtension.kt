package com.pucminas.schedule.web.extension

import com.pucminas.schedule.domain.entity.HealthInsurance
import com.pucminas.schedule.domain.entity.Plan
import com.pucminas.schedule.web.dto.request.HealthInsuranceCreateRequest
import com.pucminas.schedule.web.dto.request.HealthInsuranceUpdateRequest
import com.pucminas.schedule.web.dto.response.HealthInsuranceResponse
import java.time.LocalDateTime
import java.util.*

fun HealthInsuranceCreateRequest.toEntity(plan: Plan) = HealthInsurance(
    id = UUID.randomUUID().toString(),
    plan = plan,
    associateId = this.associateId,
    company = null,
    note = this.note,
    isActive = this.isActive,
    appointmentsServiceLimit = this.appointmentsServiceLimit,
    serviceLimitExams = this.serviceLimitExams,
    contractStartDate = this.contractStartDate,
    contractEndDate = this.contractEndDate,
    isRenewedContract = this.isRenewedContract,
    createdAt = LocalDateTime.now()
)

fun HealthInsuranceUpdateRequest.toEntity(id: String, plan: Plan) = HealthInsurance(
    id = id,
    plan = plan,
    associateId = this.associateId,
    company = null,
    note = this.note,
    isActive = this.isActive,
    appointmentsServiceLimit = this.appointmentsServiceLimit,
    serviceLimitExams = this.serviceLimitExams,
    contractStartDate = this.contractStartDate,
    contractEndDate = this.contractEndDate,
    isRenewedContract = this.isRenewedContract,
    createdAt = this.createdAt,
    updatedAt = LocalDateTime.now()
)

fun HealthInsurance.toResponse() = HealthInsuranceResponse(
    this.id,
    this.plan.toResponse(),
    this.associateId,
    this.company?.toResponse(),
    this.note,
    isActive,
    appointmentsServiceLimit,
    serviceLimitExams,
    contractStartDate,
    contractEndDate,
    isRenewedContract
)