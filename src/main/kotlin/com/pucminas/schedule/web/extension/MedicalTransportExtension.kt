package com.pucminas.schedule.web.extension

import com.pucminas.schedule.domain.entity.MedicalTransport
import com.pucminas.schedule.web.dto.response.MedicalTransportResponse

fun MedicalTransport.toResponse() = MedicalTransportResponse(
    this.id,
    this.providerId,
    this.schedule.toResponse(),
    this.note,
    this.dateTimeFinalized,
    this.street,
    this.complement,
    this.latitude,
    this.longitude,
    this.createdAt,
    this.updatedAt
)