package com.pucminas.schedule.web.extension

import com.pucminas.schedule.domain.entity.HealthInsurance
import com.pucminas.schedule.domain.entity.Schedule
import com.pucminas.schedule.domain.entity.TypeService
import com.pucminas.schedule.web.dto.request.ScheduleRequestCreate
import com.pucminas.schedule.web.dto.request.ScheduleRequestUpdate
import com.pucminas.schedule.web.dto.response.ScheduleResponse


fun ScheduleRequestCreate.toEntity(typeService: TypeService, healthInsurance: HealthInsurance) = Schedule(
    typeService = typeService,
    partneredId = this.partneredId,
    healthInsurance = healthInsurance,
    collaboratorRegistration = this.collaboratorRegistration,
    changeCollaborator = this.changeCollaborator,
    collaboratorAuthorizedDenied = this.collaboratorAuthorizedDenied,
    isReturn = this.isReturn,
    note = this.note,
    dateTimeSchedule = this.dateTimeSchedule,
    isAuthorized = this.isAuthorized,
    isAccomplished = this.isAccomplished,
    isEmergency = this.isEmergency
)

fun ScheduleRequestUpdate.toEntity(id: String, typeService: TypeService, healthInsurance: HealthInsurance) = Schedule(
    id = id,
    typeService = typeService,
    partneredId = this.partneredId,
    healthInsurance = healthInsurance,
    collaboratorRegistration = this.collaboratorRegistration,
    changeCollaborator = this.changeCollaborator,
    collaboratorAuthorizedDenied = this.collaboratorAuthorizedDenied,
    isReturn = this.isReturn,
    note = this.note,
    dateTimeSchedule = this.dateTimeSchedule,
    isAuthorized = this.isAuthorized,
    isAccomplished = this.isAccomplished,
    isEmergency = this.isEmergency,
    createdAt = this.createdAt
)

fun Schedule.toResponse() = ScheduleResponse(
    id = this.id,
    typeService =  this.typeService.toResponse(),
    partneredId = this.partneredId,
    healthInsurance = this.healthInsurance.toResponse(),
    collaboratorRegistration = collaboratorRegistration,
    changeCollaborator = changeCollaborator,
    collaboratorAuthorizedDenied = collaboratorAuthorizedDenied,
    isReturn = this.isReturn,
    note = this.note,
    dateTimeSchedule = this.dateTimeSchedule,
    isAuthorized = isAuthorized,
    isAccomplished = this.isAccomplished,
    isEmergency = this.isEmergency,
    createdAt = this.createdAt,
    updatedAt = this.updatedAt
)