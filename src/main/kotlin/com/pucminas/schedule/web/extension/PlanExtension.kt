package com.pucminas.schedule.web.extension

import com.pucminas.schedule.domain.entity.Plan
import com.pucminas.schedule.web.dto.response.PlanResponse

fun Plan.toResponse() = PlanResponse(
    id = this.id,
    description = this.description,
    planType = this.planType,
    relationshipType = this.relationshipType,
    segmentType = this.segmentType,
    isActive = this.isActive,
    createdAt = this.createdAt,
    updatedAt = this.updatedAt
)