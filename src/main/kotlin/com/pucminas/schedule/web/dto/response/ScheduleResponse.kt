package com.pucminas.schedule.web.dto.response

import java.time.LocalDateTime

class ScheduleResponse (
    val id: String,
    val typeService: TypeServiceResponse,
    val partneredId: String,
    val healthInsurance: HealthInsuranceResponse,
    val collaboratorRegistration: Int,
    val changeCollaborator: Int,
    val collaboratorAuthorizedDenied: Int,
    val isReturn: Boolean = true,
    var note: String? = null,
    var dateTimeSchedule: LocalDateTime? = null,
    val isAuthorized: Boolean = true,
    val isAccomplished: Boolean = true,
    val isEmergency: Boolean = true,
    val createdAt: LocalDateTime? = null,
    var updatedAt: LocalDateTime? = null
)