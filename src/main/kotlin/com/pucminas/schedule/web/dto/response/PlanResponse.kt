package com.pucminas.schedule.web.dto.response

import java.time.LocalDateTime

data class PlanResponse (
    val id: String,
    val description: String,
    val planType: String,
    val relationshipType: String,
    val segmentType: String,
    val isActive: Boolean = true,
    val createdAt: LocalDateTime? = null,
    var updatedAt: LocalDateTime? = null
)