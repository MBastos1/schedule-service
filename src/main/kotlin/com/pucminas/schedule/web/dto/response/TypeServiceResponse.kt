package com.pucminas.schedule.web.dto.response

import java.time.LocalDateTime

class TypeServiceResponse (
    val id: String,
    val description: String,
    val detail: String,
    val isActive: Boolean = true,
    val createdAt: LocalDateTime? = null,
    var updatedAt: LocalDateTime? = null
)