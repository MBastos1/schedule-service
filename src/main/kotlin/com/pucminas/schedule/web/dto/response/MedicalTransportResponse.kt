package com.pucminas.schedule.web.dto.response

import java.time.LocalDateTime

class MedicalTransportResponse (
    val id: String,
    val providerId: String,
    val schedule: ScheduleResponse,
    var note: String? = null,
    var dateTimeFinalized: LocalDateTime,
    var street: String? = null,
    var complement: String? = null,
    val latitude: String?,
    val longitude: String?,
    val createdAt: LocalDateTime? = null,
    var updatedAt: LocalDateTime? = null
)