package com.pucminas.schedule.web.dto.response

import java.time.LocalDateTime

class AppointmentResponse (
    val id: String,
    val providerId: String,
    val schedule: ScheduleResponse,
    var note: String? = null,
    var dateTimeService: LocalDateTime,
    val isCallCenter: Boolean = true
)