package com.pucminas.schedule.web.dto.response

import java.time.LocalDateTime

class HealthInsuranceResponse (
    val id: String,
    val plan: PlanResponse,
    val associateId: String,
    val company: CompanyResponse?,
    var note: String? = null,
    val isActive: Boolean = true,
    val appointmentsServiceLimit: Int,
    val serviceLimitExams: Int,
    val contractStartDate: LocalDateTime,
    val contractEndDate: LocalDateTime? = null,
    val isRenewedContract: Boolean = true,
    val createdAt: LocalDateTime? = null,
    var updatedAt: LocalDateTime? = null
)