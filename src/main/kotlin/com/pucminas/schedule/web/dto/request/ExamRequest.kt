package com.pucminas.schedule.web.dto.request

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonInclude
import java.time.LocalDateTime
import java.util.*

@JsonInclude(JsonInclude.Include.NON_NULL)
class ExamRequest (
    @JsonIgnore val id: String = UUID.randomUUID().toString(),
    val providerId: String,
    val scheduleId: String,
    var note: String? = null,
    var dateTimeService: LocalDateTime
)