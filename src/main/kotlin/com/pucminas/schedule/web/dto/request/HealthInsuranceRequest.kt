package com.pucminas.schedule.web.dto.request

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonInclude
import com.pucminas.schedule.web.dto.response.CompanyResponse
import java.time.LocalDateTime

@JsonInclude(JsonInclude.Include.NON_NULL)
class HealthInsuranceCreateRequest (
    val planId: String,
    val associateId: String,
    val companyId: String? = null,
    var note: String?,
    val isActive: Boolean = true,
    val appointmentsServiceLimit: Int,
    val serviceLimitExams: Int,
    val contractStartDate: LocalDateTime,
    val contractEndDate: LocalDateTime?,
    val isRenewedContract: Boolean
)

@JsonInclude(JsonInclude.Include.NON_NULL)
class HealthInsuranceUpdateRequest (
    @JsonIgnore val id: String,
    val planId: String,
    val associateId: String,
    val companyId: String?,
    var note: String?,
    val isActive: Boolean = true,
    val appointmentsServiceLimit: Int,
    val serviceLimitExams: Int,
    val contractStartDate: LocalDateTime,
    val contractEndDate: LocalDateTime?,
    val isRenewedContract: Boolean,
    val createdAt: LocalDateTime
)
