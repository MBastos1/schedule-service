package com.pucminas.schedule.web.dto.response

import java.time.LocalDateTime

class ExamResponse (
    val id: String,
    val providerId: String,
    val schedule: ScheduleResponse,
    var note: String? = null,
    var dateTimeService: LocalDateTime
)