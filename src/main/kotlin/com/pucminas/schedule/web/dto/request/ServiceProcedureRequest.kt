package com.pucminas.schedule.web.dto.request

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonInclude

@JsonInclude(JsonInclude.Include.NON_NULL)
class ServiceProcedureRequest (
    val procedureId: String,
    val scheduleId: String,
    val description: String,
    val code: String
)