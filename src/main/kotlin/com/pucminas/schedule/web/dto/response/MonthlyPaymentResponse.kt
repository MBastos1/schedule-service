package com.pucminas.schedule.web.dto.response

import java.time.LocalDateTime

class MonthlyPaymentResponse (
    val id: String,
    val healthInsurance: HealthInsuranceResponse,
    var note: String? = null,
    val monthlyAmount: Double,
    val parcelNumber: Int,
    val dueDate: LocalDateTime,
    val paymentDate: LocalDateTime,
    val isActive: Boolean = true,
    val isPaid: Boolean = false,
    val extraPercentage: Int,
    val createdAt: LocalDateTime? = null,
    var updatedAt: LocalDateTime? = null
)