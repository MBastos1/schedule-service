package com.pucminas.schedule.web.dto.request

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonInclude
import java.time.LocalDateTime

@JsonInclude(JsonInclude.Include.NON_NULL)
data class ScheduleRequestCreate (
    val typeServiceId: String,
    val partneredId: String,
    val healthInsuranceId: String,
    val collaboratorRegistration: Int,
    val changeCollaborator: Int,
    val collaboratorAuthorizedDenied: Int,
    val isReturn: Boolean = true,
    var note: String? = null,
    var dateTimeSchedule: LocalDateTime? = null,
    val isAuthorized: Boolean = true,
    val isAccomplished: Boolean = true,
    val isEmergency: Boolean = true
)

@JsonInclude(JsonInclude.Include.NON_NULL)
data class ScheduleRequestUpdate (
    @JsonIgnore val id: String? = null,
    val typeServiceId: String,
    val partneredId: String,
    val healthInsuranceId: String,
    val collaboratorRegistration: Int,
    val changeCollaborator: Int,
    val collaboratorAuthorizedDenied: Int,
    val isReturn: Boolean = true,
    var note: String? = null,
    var dateTimeSchedule: LocalDateTime? = null,
    val isAuthorized: Boolean = true,
    val isAccomplished: Boolean = true,
    val isEmergency: Boolean = true,
    val createdAt: LocalDateTime
)