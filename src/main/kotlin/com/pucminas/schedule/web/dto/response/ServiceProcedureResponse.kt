package com.pucminas.schedule.web.dto.response

import java.time.LocalDateTime

class ServiceProcedureResponse (
    val id: String,
    val procedure: ProcedureResponse,
    val schedule: ScheduleResponse,
    val description: String,
    val code: String,
    val createdAt: LocalDateTime? = null
)