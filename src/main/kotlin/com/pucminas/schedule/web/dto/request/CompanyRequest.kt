package com.pucminas.schedule.web.dto.request

class CompanyRequest (
    val socialReason: String,
    val fantasyName: String,
    val cnpj: String,
    val contactName: String,
    val contactPhone: String,
    val address: String,
    val isActive: Boolean = false,
)
