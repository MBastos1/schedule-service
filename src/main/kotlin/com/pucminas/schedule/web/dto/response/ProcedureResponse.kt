package com.pucminas.schedule.web.dto.response

import java.time.LocalDateTime

class ProcedureResponse (
    val id: String,
    val description: String,
    val detail: String,
    val code: String,
    val createdAt: LocalDateTime? = null,
    var updatedAt: LocalDateTime? = null
)