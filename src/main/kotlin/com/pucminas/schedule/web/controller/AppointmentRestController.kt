package com.pucminas.schedule.web.controller

import com.pucminas.schedule.web.service.AppointmentService
import io.swagger.annotations.Api
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@Api(description = "Serviço de Consulta")
@RestController
@RequestMapping(AppointmentRestController.URL)
class AppointmentRestController(
    private val appointmentService: AppointmentService
){

    companion object {
        const val URL = "/appointment"
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    fun getAll() = appointmentService.findAll()

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    fun findById(@PathVariable id: String) = appointmentService.findById(id)
}