package com.pucminas.schedule.web.controller

import com.pucminas.schedule.web.service.MonthlyPaymentService
import io.swagger.annotations.Api
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@Api(description = "Serviço de Mensalidades")
@RestController
@RequestMapping(MonthlyPaymentRestController.URL)
class MonthlyPaymentRestController(
    private val monthlyPaymentService: MonthlyPaymentService
) {

    companion object {
        const val URL = "/monthly-payment"
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    fun getAll() = monthlyPaymentService.findAll()

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    fun findById(@PathVariable id: String) = monthlyPaymentService.findById(id)
}