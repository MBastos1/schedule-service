package com.pucminas.schedule.web.controller

import com.pucminas.schedule.web.service.MedicalTransportService
import io.swagger.annotations.Api
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@Api(description = "Serviço de Transporte Médico")
@RestController
@RequestMapping(MedicalTransportRestController.URL)
class MedicalTransportRestController(
    private val medicalTransportService: MedicalTransportService
) {

    companion object {
        const val URL = "/medical-transport"
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    fun getAll() = medicalTransportService.findAll()

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    fun findById(@PathVariable id: String) = medicalTransportService.findById(id)
}