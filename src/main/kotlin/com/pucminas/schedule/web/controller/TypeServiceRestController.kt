package com.pucminas.schedule.web.controller

import com.pucminas.schedule.web.service.TypeService
import io.swagger.annotations.Api
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@Api(description = "Serviço de Tipos de Atendimento")
@RestController
@RequestMapping(TypeServiceRestController.URL)
class TypeServiceRestController(private val typeService: TypeService) {

    companion object {
        const val URL = "/type-service"
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    fun getAll() = typeService.findAll()

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    fun findById(@PathVariable id: String) = typeService.findById(id)
}