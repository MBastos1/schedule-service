package com.pucminas.schedule.web.controller

import com.pucminas.schedule.web.dto.request.ExamRequest
import com.pucminas.schedule.web.dto.request.ServiceProcedureRequest
import com.pucminas.schedule.web.service.ServiceProcedureService
import io.swagger.annotations.Api
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@Api(description = "Serviço de Procedimentos do Atendimento")
@RestController
@RequestMapping(ServiceProcedureRestController.URL)
class ServiceProcedureRestController(
    private val serviceProcedureService: ServiceProcedureService
) {

    companion object {
        const val URL = "/service-procedure"
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun save(@RequestBody serviceProcedureRequest: ServiceProcedureRequest) =
        serviceProcedureService.save(serviceProcedureRequest)

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    fun getAll() = serviceProcedureService.findAll()

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    fun findById(@PathVariable id: String) = serviceProcedureService.findById(id)
}