package com.pucminas.schedule.web.controller

import com.pucminas.schedule.web.service.ProcedureService
import io.swagger.annotations.Api
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@Api(description = "Serviço de Procedimentos")
@RestController
@RequestMapping(ProcedureRestController.URL)
class ProcedureRestController(private val procedureService: ProcedureService) {

    companion object {
        const val URL = "/procedure"
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    fun getAll() = procedureService.findAll()

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    fun findById(@PathVariable id: String) = procedureService.findById(id)
}