package com.pucminas.schedule.web.controller

import com.pucminas.schedule.web.service.CompanyService
import io.swagger.annotations.Api
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@Api(description = "Serviço de Empresas")
@RestController
@RequestMapping(CompanyRestController.URL)
class CompanyRestController(private val companyService: CompanyService) {

    companion object {
        const val URL = "/company"
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    fun getAll() = companyService.findAll()

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    fun findById(@PathVariable id: String) = companyService.findById(id)
}