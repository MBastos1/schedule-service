package com.pucminas.schedule.web.controller

import com.pucminas.schedule.web.dto.request.ExamRequest
import com.pucminas.schedule.web.service.ExamService
import io.swagger.annotations.Api
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@Api(description = "Serviço de Exames")
@RestController
@RequestMapping(ExamRestController.URL)
class ExamRestController(private val examService: ExamService) {

    companion object {
        const val URL = "/exam"
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun save(@RequestBody examRequest: ExamRequest) =
        examService.save(examRequest)

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    fun edit(@PathVariable id: String, @RequestBody examRequest: ExamRequest) =
        examService.edit(id, examRequest)

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    fun getAll() = examService.findAll()

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    fun findById(@PathVariable id: String) = examService.findById(id)

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    fun deleteById(@PathVariable id: String) = examService.deleteById(id)
}