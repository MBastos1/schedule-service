package com.pucminas.schedule.web.controller

import com.pucminas.schedule.web.dto.request.ExamRequest
import com.pucminas.schedule.web.dto.request.HealthInsuranceCreateRequest
import com.pucminas.schedule.web.dto.request.HealthInsuranceUpdateRequest
import com.pucminas.schedule.web.service.HealthInsuranceService
import io.swagger.annotations.Api
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@Api(description = "Serviço de Convênio")
@RestController
@RequestMapping(HealthInsuranceRestController.URL)
class HealthInsuranceRestController(
    private val healthInsuranceService: HealthInsuranceService
) {
    companion object {
        const val URL = "/health-insurance"
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun save(@RequestBody healthInsuranceCreateRequest: HealthInsuranceCreateRequest) =
        healthInsuranceService.save(healthInsuranceCreateRequest)

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    fun edit(@PathVariable id: String, @RequestBody healthInsuranceUpdateRequest: HealthInsuranceUpdateRequest) =
        healthInsuranceService.edit(id, healthInsuranceUpdateRequest)

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    fun getAll() = healthInsuranceService.findAll()

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    fun findById(@PathVariable id: String) = healthInsuranceService.findById(id)
}