package com.pucminas.schedule.web.controller

import com.pucminas.schedule.web.service.PlanService
import io.swagger.annotations.Api
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@Api(description = "Serviço de Planos")
@RestController
@RequestMapping(PlanRestController.URL)
class PlanRestController(private val planService: PlanService) {

    companion object {
        const val URL = "/plan"
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    fun getAll() = planService.findAll()

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    fun findById(@PathVariable id: String) = planService.findById(id)

}