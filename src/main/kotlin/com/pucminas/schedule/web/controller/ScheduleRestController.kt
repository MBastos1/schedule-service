package com.pucminas.schedule.web.controller

import com.pucminas.schedule.web.dto.request.HealthInsuranceCreateRequest
import com.pucminas.schedule.web.dto.request.HealthInsuranceUpdateRequest
import com.pucminas.schedule.web.dto.request.ScheduleRequestCreate
import com.pucminas.schedule.web.dto.request.ScheduleRequestUpdate
import com.pucminas.schedule.web.service.ScheduleService
import io.swagger.annotations.Api
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*


@Api(description = "Serviço de Agendamento")
@RestController
@RequestMapping(ScheduleRestController.URL)
class ScheduleRestController(private val scheduleService: ScheduleService) {

    companion object {
        const val URL = "/schedule"
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun save(@RequestBody scheduleRequestCreate: ScheduleRequestCreate) =
        scheduleService.save(scheduleRequestCreate)

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    fun edit(@PathVariable id: String, @RequestBody scheduleRequestUpdate: ScheduleRequestUpdate) =
        scheduleService.edit(id, scheduleRequestUpdate)

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    fun getAll() = scheduleService.findAll()

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    fun findById(@PathVariable id: String) = scheduleService.findById(id)

}