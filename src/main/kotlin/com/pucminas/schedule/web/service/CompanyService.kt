package com.pucminas.schedule.web.service

import com.pucminas.schedule.domain.base.BaseService
import com.pucminas.schedule.web.dto.request.CompanyRequest
import com.pucminas.schedule.web.dto.response.CompanyResponse

interface CompanyService : BaseService<CompanyResponse, String>