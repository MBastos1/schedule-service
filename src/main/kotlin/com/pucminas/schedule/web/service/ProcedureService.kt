package com.pucminas.schedule.web.service

import com.pucminas.schedule.domain.base.BaseService
import com.pucminas.schedule.web.dto.response.ProcedureResponse

interface ProcedureService : BaseService<ProcedureResponse, String>