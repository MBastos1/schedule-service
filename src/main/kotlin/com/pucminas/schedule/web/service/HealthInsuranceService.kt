package com.pucminas.schedule.web.service

import com.pucminas.schedule.domain.base.BaseService
import com.pucminas.schedule.web.dto.request.HealthInsuranceCreateRequest
import com.pucminas.schedule.web.dto.request.HealthInsuranceUpdateRequest
import com.pucminas.schedule.web.dto.response.HealthInsuranceResponse

interface HealthInsuranceService : BaseService<HealthInsuranceResponse, String> {
    fun save(healthInsuranceCreateRequest: HealthInsuranceCreateRequest) : HealthInsuranceResponse
    fun edit(id: String, healthInsuranceUpdateRequest: HealthInsuranceUpdateRequest) : HealthInsuranceResponse
}