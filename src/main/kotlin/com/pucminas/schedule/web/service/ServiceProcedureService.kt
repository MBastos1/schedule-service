package com.pucminas.schedule.web.service

import com.pucminas.schedule.domain.base.BaseService
import com.pucminas.schedule.web.dto.request.ServiceProcedureRequest
import com.pucminas.schedule.web.dto.response.ServiceProcedureResponse

interface ServiceProcedureService : BaseService<ServiceProcedureResponse, String> {
    fun save(serviceProcedureRequest: ServiceProcedureRequest): ServiceProcedureResponse
}