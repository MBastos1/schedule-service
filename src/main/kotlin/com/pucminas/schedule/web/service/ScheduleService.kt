package com.pucminas.schedule.web.service

import com.pucminas.schedule.domain.base.BaseService
import com.pucminas.schedule.web.dto.request.ScheduleRequestCreate
import com.pucminas.schedule.web.dto.request.ScheduleRequestUpdate
import com.pucminas.schedule.web.dto.response.ScheduleResponse

interface ScheduleService : BaseService<ScheduleResponse, String> {
    fun save(scheduleRequestCreate: ScheduleRequestCreate): ScheduleResponse
    fun edit(id: String, scheduleRequestUpdate: ScheduleRequestUpdate): ScheduleResponse
}