package com.pucminas.schedule.web.service

import com.pucminas.schedule.domain.base.BaseService
import com.pucminas.schedule.web.dto.response.PlanResponse

interface PlanService : BaseService<PlanResponse, String>
