package com.pucminas.schedule.web.service

import com.pucminas.schedule.domain.base.BaseService
import com.pucminas.schedule.web.dto.request.ExamRequest
import com.pucminas.schedule.web.dto.response.ExamResponse

interface ExamService : BaseService<ExamResponse, String> {
    fun save(examRequest: ExamRequest): ExamResponse
    fun edit(id: String, examRequest: ExamRequest): ExamResponse
    fun deleteById(id: String)
}

